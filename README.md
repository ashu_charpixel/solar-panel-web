# FourthPartner Dashboard

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Developed using 
  * `Angular.js (1.4.9)`
  * `bower` (See the instruction to install it [here](http://bower.io/))
  * `grunt` (See the instruction to install it [here](http://gruntjs.com/installing-grunt))
  
## How to run app on local box (development) 
  * `clone repo`
  * `npm install` (if not worked then install it using sudo)
  * `bower install`
  * `(sudo) grunt serve` (switch to dev branch) will start the app in browser at http://localhost:9000/

## Testing
Running `grunt test` will run the unit tests with karma.

## Prepare for production 
   * `Merge dev branch with live` 
   * `Fixed the conflicts occurred`
   * `Switch to live branch`
   * `Merge it with dev branch` (check the config.js file where api url must point to production url) 
   * `(sudo) grunt serve1` - make sure everything is working fine.
   * `(sudo) grunt serve:dist` - your production files will be ready in dist folder.
   * push the `dist/*` code to server `html` folder  - You can write your deployment script to push it to server

## Code Structure 
Folder architecture has been structured and inspired from LoopBack client side and Ruby on Rails MVC.

   * There are following modules in the app/scripts/modules folder
         . `core`   - contains core components
         . `admin`  - accounts related components
         . `plants` - plants related components
         . `shared` - common/re-usable components.
         . `users`  - users related components.

## Code naming conventions
 Please follow the definition naming patterns used in the skeleton controllers, services and directives.
  
 `Reference:-` https://github.com/johnpapa/angular-styleguide
 
## Who do I talk to? 
  * This is firstly developed and architectured by **Govind Sah** for more info and queries contact at **er.govindsah@gmail.com** 
  * Yes you are developers we understand your problem feel free to ask :+1:
