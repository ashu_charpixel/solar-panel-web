/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc Intercept 401 responses and redirect to login screen
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .factory('RequestInterceptor', RequestInterceptor);

    RequestInterceptor.$inject = ['$q','$location','$cookieStore', '$rootScope'];
    function RequestInterceptor($q, $location, $cookieStore, $rootScope){
      var service = {};

      service.responseError = responseError;

      return service;

      function responseError(rejection){

        if (rejection.status === 401) {

          // save the current location so that login can redirect back
          $location.nextAfterLogin = $location.path();

          $cookieStore.put('responseErrorMsg','Your session has been expired!, Redirecting to login.');
          $rootScope.$broadcast('unauthorized');
        }

        if (rejection.status === 400) {
          $cookieStore.put('responseErrorMsg', rejection.data.message);
          $rootScope.$broadcast('bad_request');
        }

        if (rejection.status === 500) {
          $cookieStore.put('responseErrorMsg','Oops! Something went wrong, please try again later.');
          $rootScope.$broadcast('something_went_wrong');
        }

        return $q.reject(rejection);
      }

    }

})();
