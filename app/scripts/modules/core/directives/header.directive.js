/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc custom header directive
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .directive("customHeader", customHeader);

    function customHeader() {
      var directive = {
        restrict: 'E',
        templateUrl: 'scripts/modules/core/views/header.html',
        replace: true,
        scope: {
          admin: "=",
          projectName : "=",
          logout: "&"
        }

      };


      return directive;

    }
})();
