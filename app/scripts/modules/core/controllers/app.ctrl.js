/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold common data of whole page section
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .controller('AppCtrl', AppCtrl);

  AppCtrl.$inject = ['$rootScope', 'PROJECT_DETAILS', 'AuthenticationService', 'toastr', '$location'];

  function AppCtrl($rootScope, PROJECT_DETAILS, AuthenticationService, toastr, $location) {
    var vm = this;

    //Variables declaration goes here
    vm.projectName = PROJECT_DETAILS.NAME;
    vm.admin = $rootScope.globals.currentAdmin;

    // Functions declaration goes here
    vm.logout = logout;

    function logout() {
      AuthenticationService.Logout()
        .then(function (response) {
          if (response.success) {
            AuthenticationService.ClearCredentials();
            toastr.success('You have successfully logged out');
            $location.path('/');
          }
        });
    }

  }

})();
