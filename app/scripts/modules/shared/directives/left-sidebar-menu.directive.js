/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc left sidebar directive
 */

(function() {
    'use strict';

    angular
        .module('com.module.shared')
        .directive("leftSidebarDirective", leftSidebarDirective);

    function leftSidebarDirective() {
        var directive = {
            restrict: 'EA',
            templateUrl: 'scripts/modules/shared/partials/left-sidebar-menu.html',
            replace: true,
            scope: {
                admin: "="
            },
            controller: LeftSidebarCtrl,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

    }

    LeftSidebarCtrl.$inject = ['toastr', '$http', '$state', '$rootScope'];

    function LeftSidebarCtrl(toastr, $http, $state, $rootScope) {
        var vm = this;
        var user = $rootScope.globals.currentAdmin;

        vm.state = true;
        vm.toggleState = toggleState;
        vm.loadSidebarMenu = loadSidebarMenu;
        vm.getMenuItemPropClasses = getMenuItemPropClasses;
        vm.click = click;

        (function initController() {
            loadSidebarMenu();
        })();


        // Check item and children active state
        var isActive = function(item) {

            if (!item) return;

            if (!item.sref) {
                var foundActive = false;
                angular.forEach(item.submenu, function(value, key) {
                    if (isActive(value)) foundActive = true;
                });

                return foundActive;
            } else {
                return $state.is(item.sref) || $state.includes(item.sref);
            }
        };

        function permissionFilter(item) {

            if (item.permissionId == null) {
                return true;
            } else if (user.permissions[item.permissionId] && user.permissions[item.permissionId] == true) {
                return true;
            } else {
                return false;
            }
        }

        function loadSidebarMenu() {
            var menuJson = 'jsons/sidebar-menu.json';
            var menuURL = menuJson + '?v=' + (new Date().getTime()); // jumps cache

            $http.get(menuURL)
                .success(function(items) {

                    var filteredMenuitems = [];

                    items.forEach(function(item) { // set isMenuExpanded once loaded the sidebar menu

                        if (!item.sref) {

                            item.submenu = item.submenu.filter(permissionFilter);
                            angular.forEach(item.submenu, function(value, key) {
                                if (isActive(value)) item.isMenuExpanded = true;
                            });
                        }

                        if (!(item.permissionId && user.permissions[item.permissionId] == false)) {
                            filteredMenuitems.push(item);
                        }
                    });

                    vm.menuItems = filteredMenuitems;
                })
                .error(function() {
                    toastr.warning('Failure loading menu');
                });
        }

        function toggleState() {
            vm.state = !vm.state;
        }

        function getMenuItemPropClasses(item) {
            return (isActive(item) ? ' active' : '');
        }

        function click(item, event) {
            if (!item.sref) {
                event.preventDefault();
                item.isMenuExpanded = !item.isMenuExpanded;
            } else {
                vm.menuItems.forEach(function(innerItem) {
                    if (innerItem.isMenuExpanded)
                        innerItem.isMenuExpanded = false;
                });
            }
        }
    }

})();
