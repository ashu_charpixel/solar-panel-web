 /**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc back button directive
 */

(function () {
  'use strict';

  angular
    .module('com.module.shared')
    .directive("backButton", backButton);

  function backButton() {
    var directive = {
      restrict: 'A',
      link: link
    };

    return directive;

    function link(scope, element, attrs){
      element.bind('click', goBack);

      function goBack() {
        history.back();
        scope.$apply();
      }
    }

  }
})();
