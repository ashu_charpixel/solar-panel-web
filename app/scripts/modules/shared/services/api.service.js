/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc user related services
 */


(function() {
    'use strict';

    angular
        .module('com.module.shared')
        .factory('ApiService', ApiService);

    ApiService.$inject = ['$http', 'ENV'];

    function ApiService($http, ENV) {
        var baseUrl = ENV.apiUrl;

        var service = {
            GetAll: GetAll,
            GetCSV: GetCSV,
            GetById: GetById,
            Create: Create,
            Update: Update,
            Delete: Delete,
            FileDownload: FileDownload
        };

        return service;

        function GetAll(path, payload) {
            return $http.get(baseUrl + path, { params: payload }).then(handleSuccess, handleError);
        }

        function GetCSV(path) {
            return $http.get(baseUrl + path, { timeout: 1200000, responseType: 'arraybuffer' }).then(handleSuccess, handleError);
        }

        function GetById(payload, path) {
            return $http.get(baseUrl + path, { params: payload }).then(handleSuccess, handleError);
        }

        function Create(payload, path) {
            return $http.post(baseUrl + path, payload).then(handleSuccess, handleError);
        }

        function Update(payload, path) {
            return $http.put(baseUrl + path, payload).then(handleSuccess, handleError);
        }

        function Delete(path) {
            return $http.delete(baseUrl + path).then(handleSuccess, handleError);
        }

        function FileDownload(res, name) {
            var blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation;charset=UTF-8' });
            var objectUrl = (window.URL || window.webkitURL).createObjectURL(blob);
            var link = angular.element('<a/>');
            link.attr({
                href: objectUrl,
                download: name + ".xlsx"
            })[0].click();
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError() {
            return { success: false };
        }

    }

})();
