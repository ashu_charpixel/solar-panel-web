/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold all the dialog box
 */

(function () {
  'use strict';

  angular
    .module('com.module.shared')
    .factory('DialogBoxService', DialogBoxService);

  DialogBoxService.$inject = ['ngDialog'];
  function DialogBoxService(ngDialog) {
    var service = {};

    service.Close = Close;
    service.CustomerReview = CustomerReview;
    service.MerchantReview = MerchantReview;

    return service;

    function Close() {
      ngDialog.closeAll();
    }

    function CustomerReview(){
      ngDialog.open({
        template: 'scripts/modules/homes/partials/customer-review.html',
        showClose: false,
        closeByEscape: false,
        closeByDocument: false,
        controller: 'CustomersReviewCtrl',
        controllerAs: 'vm',
        className: 'ngdialog-theme-default fs-dialog'
      });
    }

    function MerchantReview(){
      ngDialog.open({
        template: 'scripts/modules/homes/partials/merchant-review.html',
        showClose: false,
        closeByEscape: false,
        closeByDocument: false,
        controller: 'MerchantsReviewCtrl',
        controllerAs: 'vm',
        className: 'ngdialog-theme-default fs-dialog'
      });
    }
  }

})();
