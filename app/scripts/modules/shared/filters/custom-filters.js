/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc global filters goes here
 */

(function () {
  'use strict';

  angular
    .module('com.module.shared')
    .filter("capsStrToSpace", capsStrToSpace);

  function capsStrToSpace() {
    return function(label) {
      return label.replace(/([A-Z])/g, ' $1').trim();
    };
  }
})();
