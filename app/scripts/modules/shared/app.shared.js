/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc shared module
 */

(function () {
  'use strict';
  angular.module('com.module.shared', []);

})();
