/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc Plants module of the application.
 */

(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {

    // Now set up the states
    $stateProvider
      .state('app.addPlant', {
        url: '/plants/add',
        stateDesc: { bodyClass: 'plant-page', title: 'Power Plant', description: 'Add a new power plant' },
        templateUrl: 'scripts/modules/plants/views/add-plant.html',
        controller: 'AddPlantCtrl',
        controllerAs: 'vm'
      })
      .state('app.editPlant', {
        url: '/plants/:id/edit',
        stateDesc: { bodyClass: 'plant-page', title: 'Edit Power Plant', description: 'Edit existing power plant' },
        templateUrl: 'scripts/modules/plants/views/edit-plant.html',
        controller: 'EditPlantCtrl',
        controllerAs: 'vm'
      })
      .state('app.showPlant', {
        url: '/plants/:id',
        stateDesc: { bodyClass: 'plant-page', title: 'Power Plant Details', description: 'Show power plant details' },
        templateUrl: 'scripts/modules/plants/views/show-plant.html',
        controller: 'ShowPlantCtrl',
        controllerAs: 'vm'
      })
      .state('app.addDevice', {
        url: '/plants/:id/devices',
        stateDesc: { bodyClass: 'plant-page', title: 'Device MAC', description: 'Add a Device MAC with complete details' },
        templateUrl: 'scripts/modules/plants/views/add-device.html',
        controller: 'AddDeviceCtrl',
        controllerAs: 'vm'
      })
      .state('app.editDevice', {
        url: '/plants/:id/devices/:deviceId/edit',
        stateDesc: { bodyClass: 'plant-page', title: 'Edit Device MAC', description: 'Edit a Device MAC with complete details' },
        templateUrl: 'scripts/modules/plants/views/edit-device.html',
        controller: 'EditDeviceCtrl',
        controllerAs: 'vm'
      });
  }

})();
