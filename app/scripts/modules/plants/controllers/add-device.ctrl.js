/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc add a power plant
 */

(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .controller('AddDeviceCtrl', AddDeviceCtrl);

  AddDeviceCtrl.$inject = ['$log', '$state', '$stateParams','ApiService', 'toastr'];
  function AddDeviceCtrl($log, $state, $stateParams, ApiService, toastr) {
    var vm = this;

    vm.currentPageState = $state.current.stateDesc;

    vm.payload = {};
    vm.noOfInverters = 1;
    vm.payload.inverters = [];
    vm.payload.irradiationSensorInstalled = false;

    vm.noOfMeters = 1;
    vm.payload.meters = [];
    vm.energyCalculationCriteria =[
      {
        name:'Inverter',
        value:'inverter'
      }, {
        name:'Meter',
        value:'meter'
      }, {
        name:'None',
        value:'none'
      }
    ];

    // Function Declaration goes here
    vm.addDevice = addDevice;
    vm.removeMeter = removeMeter;
    vm.removeInverter = removeInverter;

    function addDevice() {
      vm.dataLoading = true;

      vm.payload.powerPlantId = $stateParams.id;

      var rUrl = '/powerPlants/' + vm.payload.powerPlantId + '/deviceMacs';

      ApiService.Create(vm.payload, rUrl)
        .then(function (response) {
          vm.dataLoading = false;

          if (response.success) {
            toastr.success(response.message);
            $state.reload();
          }
        });
    }

    function removeMeter(index) {
      vm.payload.meters.splice(index, 1);
      vm.noOfMeters = vm.noOfMeters - 1;
    }

    function removeInverter(index) {
      vm.payload.inverters.splice(index, 1);
      vm.noOfInverters = vm.noOfInverters - 1;
    }

  }

})();
