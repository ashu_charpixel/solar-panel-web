/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc add a power plant
 */

(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .controller('EditPlantCtrl', EditPlantCtrl);

  EditPlantCtrl.$inject = ['$state', 'ApiService', '$stateParams', 'toastr','$rootScope'];
  function EditPlantCtrl($state, ApiService, $stateParams, toastr,$rootScope) {
    var vm = this;

    vm.currentPageState = $state.current.stateDesc;
    vm.permissions = $rootScope.globals.currentAdmin.permissions;

    // Function declarations goes here
    vm.updatePlant = updatePlant;
    vm.open1 = open1;
    vm.open2 = open2;

    vm.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(),
      startingDay: 1
    };

    function open1() {
      vm.popup1.opened = true;
    }

    function open2() {
      vm.popup2.opened = true;
    }

    vm.format = 'dd MMMM yyyy';

    vm.popup1 = {
      opened: false
    };

    vm.popup2 = {
      opened: false
    };

    (function initController(){
      getPlantDetails();
    }());

    function getPlantDetails() {

      var requestPayload = {
        projection: {"createdAt": 0,"updatedAt": 0,"isDeleted": 0, "__v": 0, "deviceMacLength": 0},
        criteria: {},
        options: {}
      };

      ApiService.GetById(requestPayload, '/powerPlants/'+$stateParams.id).then(function (res) {
        if (res.success) {
          vm.payload = res.data;

          var arr = vm.payload.location;

          angular.extend(vm.payload, {
            latitude: arr[0],
            longitude: arr[1],
            commissioningDate: new Date(vm.payload.commissioningDate),
            dataLoggingDate: new Date(vm.payload.dataLoggingDate)
          });

        }
      });
    }

    // Add plants functions
    function updatePlant() {
      vm.dataLoading = true;

      // Convert lat long to array and delete the latitude & longitude key
      var payload = angular.copy(vm.payload);
      delete payload.latitude;
      delete payload.longitude;
      payload.location = [vm.payload.latitude, vm.payload.longitude];

      ApiService.Update(payload, '/powerPlants/'+payload._id)
        .then(function (response) {
          vm.dataLoading = false;
          if (response.success) {
            toastr.success('Updated successfully!');
          }
        });
    }

  }

})();
