/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc show the power plants details
 */

(function () {
    'use strict';

    angular
      .module('com.module.plants')
      .controller('ShowPlantCtrl', ShowPlantCtrl);

    ShowPlantCtrl.$inject = ['PlantService', 'ENV', '$scope', '$log', '$state', 'ApiService', '$stateParams', 'ngDialog', 'toastr', '$timeout', 'DialogBoxService', '$rootScope'];

    function ShowPlantCtrl(PlantService, ENV, $scope, $log, $state, ApiService, $stateParams, ngDialog, toastr, $timeout, DialogBoxService, $rootScope) {
      var vm = this;
      var baseUrl = ENV.apiUrl;

      vm.currentPageState = $state.current.stateDesc;
      vm.obj = PlantService.sharedObj;
      vm.drawChart = drawChart;
      vm.incDecEnergyDate = incDecEnergyDate;
      vm.incDecInvDate = incDecInvDate;
      vm.incDecRawDate = incDecRawDate;
      vm.setGraphType = setGraphType;
      vm.date1Change = date1Change;
      vm.date2Change = date2Change;
      vm.date3Change = date3Change;
      vm.delete = deletePlant;
      vm.updateConfirm = updateConfirm;
      vm.remoteShutDownData = remoteShutDownData;
      vm.getRawData = getRawData;

      var user = $rootScope.globals.currentAdmin;
      vm.permissions = user.permissions;
      vm.userType = user.userType;

      // date-picker goes here
      vm.date1 = new Date();
      vm.date2 = new Date();
      vm.date3 = new Date();
      vm.type = 'month';

      vm.open1 = open1;
      vm.open2 = open2;
      vm.open3 = open3;

      vm.dateOptions1 = {
        formatYear: 'yyyy',
        maxDate: new Date(),
        minMode: 'month',
        startingDay: 1
      };

      vm.dateOptions2 = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1
      };
      vm.dateOptions3 = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1
      };

      function open1() {
        vm.popup1.opened = true;
      }

      function open2() {
        vm.popup2.opened = true;
      }

      function open3() {
        vm.popup3.opened = true;
      }

      vm.format1 = 'MMMM yyyy';
      vm.format2 = 'dd MMMM yyyy';
      vm.format3 = 'dd MMMM yyyy';

      vm.popup1 = {
        opened: false
      };

      vm.popup2 = {
        opened: false
      };
      vm.popup3 = {
        opened: false
      };

      // Get Initial Data from server
      (function initController() {
        getPlantDetails();
      }());

      function getPlantDetails() {

        var requestPayload = {
          projection: {"createdAt": 0, "updatedAt": 0, "isDeleted": 0, "__v": 0, "deviceMacLength": 0},
          criteria: {},
          options: {}
        };

        ApiService.GetById(requestPayload, '/powerPlants/details/' + $stateParams.id).then(function (res) {
          if (res.success) {
            vm.inverters = [];
            vm.meters = [];
            vm.liveInverters = [];
            vm.data = res.data;

            vm.data.deviceMac.forEach(function (device) {
              device.inverters.forEach(function (inverter) {
                vm.inverters.push(inverter);
              });
            });

            vm.data.slideDown.energyMeterNow.forEach(function (device) {
              device.meters.forEach(function (meter) {
                vm.meters.push(meter);
              });
            });

            vm.data.slideDown.invertersData.forEach(function (device) {
              device.inverters.forEach(function (inverter) {
                vm.liveInverters.push(inverter);
              });
            });

          } else {
            $state.go('app.dashboard');
          }
        });

      }

      function updateEnergyHighCharts(data) {

        vm.chartConfig1 = {
          options: {
            tooltip: {
              enabled: true,
              shared: true,
              followPointer: false,
              // borderWidth: 0,
              shadow: false,
              useHTML: true,

              // positioner: function (labelWidth, labelHeight, point) {
              //   return {
              //     x: point.plotX - labelWidth / 2,
              //     y: point.plotY - labelHeight / 2
              //   };
              // },
              // formatter: function () {
              //   var s = '<div class="myChartTooltip">';
              //   s += '<b>' + this.x + '</b>';
              //   $.each(this.points, function () {
              //     s += '<br/> <span class="fa fa-circle" style="font-size:0.7em;margin-right:2px;color:'+this.series.color+'"></span>' + this.series.name + ': ' +this.y+ '<b> kWh</b>';
              //     // s += '<br/>' + this.series.name + ': ' + this.y + '<b> kWh</b>';
              //   });
              //   s += '</div>'
              //   return s;
              // }
            },
          },
          title: {
            text: ''
          },
          xAxis: [{
            categories: data.date,
            crosshair: true
          }],
          yAxis: [{ // Primary yAxis
            gridLineWidth: 0,
            title: {
              text: 'Energy'
            },
            labels: {
              format: '{value} kWh'
            }
          }, { // Secondary yAxis
            labels: {
              format: '{value} %'
            },
            title: {
              text: 'PR'
            },
            min: 0,
            max: 110,
            opposite: true
          }],
          legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          series: [{
            name: 'PR',
            type: 'spline',
            data: data.pr,
            yAxis: 1,
            color: Highcharts.getOptions().colors[0],
            marker: {
              enabled: false
            },
            tooltip: {
              valueSuffix: ' %'
            }
          }, {
            name: 'Energy (Meters)',
            type: 'column',
            data: data.meters,
            color: Highcharts.getOptions().colors[1],
            tooltip: {
              valueSuffix: ' kWh'
            }

          }
          ],
          loading: false
        };

        var i = 2;

        data.inverterArray.forEach(function (inverter) {

          angular.extend(inverter, {
            tooltip: {
              valueSuffix: ' kWh'
            },
            color: Highcharts.getOptions().colors[i]
          });

          vm.chartConfig1.series.push(inverter);

          i = i + 1;

        });
        if (data.gridDeviceMAC && data.gridDeviceMAC != "") {
          var j = {
            name: 'Grid Import',
            type: 'column',
            data: data.meter_kwh_import,
            color: Highcharts.getOptions().colors[11],
            tooltip: {
              valueSuffix: ' kWh'
            }
          };

          var e = {
            name: 'Grid Export',
            type: 'column',
            data: data.meter_kwh_total,
            color: Highcharts.getOptions().colors[12],
            tooltip: {
              valueSuffix: ' kWh'
            }
          }
          vm.chartConfig1.series.push(j);
          vm.chartConfig1.series.push(e);
        }

      }


      function updateGridImportCHarts(data) {

        vm.chartConfig3 = {
          options: {
            tooltip: {
              shared: true
            },
            style: {
              width: '100%'
            },
            exporting: {enabled: false}
          },
          title: {
            text: ''
          },
          xAxis: [{
            categories: data.date,
            crosshair: true
          }],
          yAxis: [{ // Primary yAxis
            gridLineWidth: 0,
            title: {
              text: 'Energy'
            },
            labels: {
              format: '{value} kWh'
            }
          }, { // Secondary yAxis
            labels: {
              format: '{value} %'
            },
            title: {
              text: 'PR'
            },
            min: 0,
            max: 110,
            opposite: true
          }],
          legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          series: [{
            name: 'Meter kWH Import',
            type: 'spline',
            data: data.meter_kwh_import,
            yAxis: 1,
            color: Highcharts.getOptions().colors[0],
            marker: {
              enabled: false
            },
            tooltip: {
              valueSuffix: ' kWh'
            }
          }, {
            name: 'Energy (Meters)',
            type: 'column',
            data: data.meters,
            color: Highcharts.getOptions().colors[1],
            tooltip: {
              valueSuffix: ' kWh'
            }

          },
            {
              name: 'Meter kWH Total',
              type: 'column',
              data: data.meter_kwh_total,
              color: Highcharts.getOptions().colors[2],
              tooltip: {
                valueSuffix: ' kWh'
              }
            },
          ],
          loading: false
        };

        var i = 2;

        // data.meter_kwh_import.forEach(function(inverter) {
        //
        //   angular.extend(inverter, {
        //     tooltip: {
        //       valueSuffix: ' kWh'
        //     },
        //     color: Highcharts.getOptions().colors[i]
        //   });
        //
        //   vm.chartConfig3.series.push(inverter);
        //
        //   i = i + 1;
        //
        // });
      }

      function getEnergyAndPower() {
        if (angular.isDefined(vm.chartConfig1)) {
          vm.chartConfig1.loading = true;
        }

        var requestPayload = {
          graphType: vm.type,
          date: vm.date1
        };

        ApiService.GetById(requestPayload, '/powerPlants/graph/' + $stateParams.id).then(function (res) {
          if (res.success) {
            updateEnergyHighCharts(res.data);
            // updateGridImportCHarts(res.data);
            if (angular.isDefined(vm.chartConfig1)) {
              vm.chartConfig1.loading = false;
            }
          }
        });
      }

      function updateInvertersCharts(data) {

        vm.chartConfig2 = {
          options: {
            tooltip: {
              enabled: true,
              shared: true,
              followPointer: false,
              // borderWidth: 0,
              shadow: false,
              useHTML: true,

              // positioner: function (labelWidth, labelHeight, point) {
              //   return {
              //     x: point.plotX - labelWidth / 2,
              //     y: point.plotY - labelHeight / 2
              //   };
              // },
              // formatter: function () {
              //   var s = '<div class="myChartTooltip">';
              //   s += '<b>' + this.x + '</b>';
              //   $.each(this.points, function () {
              //     s += '<br/> <span class="fa fa-circle" style="font-size:0.7em;margin-right:2px;color:'+this.series.color+'"></span>' + this.series.name + ': ' +this.y+ '<b> kWh</b>';
              //   });
              //   s += '</div>'
              //   return s;
              // }
            },
            exporting: {enabled: false}
          },
          // chart:{
          //   alignTicks: false
          // },
          title: {
            text: ''
          },
          xAxis: [{
            categories: data.date,
            crosshair: true
          }],
          yAxis: [{ // Primary yAxis
            gridLineWidth: 0,
            title: {
              text: 'Inverters'
            },
            labels: {
              format: '{value} kW'
            },
            // linkedTo:0,
            opposite: false
          }, { // seconDARY yAxis
            gridLineWidth: 0,
            title: {
              text: 'Irradiation'
            },
            labels: {
              format: '{value} w/m2'
            },
            // linkedTo:0,
            opposite: true
          }],
          legend: {
            layout: 'vertical',
            align: 'left',
            x: 80,
            verticalAlign: 'top',
            y: 55,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
          },
          series: [],
          loading: false,
          func: function (chart) {

            $timeout(function () {
              // var  yAxis0Extremes = chart.yAxis[0].getExtremes();
              // var yAxisMaxMinRatio = yAxis0Extremes.max / yAxis0Extremes.min;
              // var yAxis1Extremes = chart.yAxis[1].getExtremes();
              // var yAxis1Min = (yAxis1Extremes.max / yAxisMaxMinRatio).toFixed(0);
              // chart.yAxis[0].setExtremes(yAxis1Min, yAxis1Extremes.max);
              var i = 25;
              while (chart.yAxis[0].translate(0) != chart.yAxis[1].translate(0) && i > 0) {
                chart.yAxis[1].setExtremes(chart.yAxis[1].getExtremes().min - chart.yAxis[1].translate(chart.yAxis[0].translate(0), true));
                i--;
              }
              ;
            });


            // var yAxis1MaxMinRatio = yAxis1Extremes.max / yAxis1Extremes.min;
            // var yAxis0Min =  (yAxis0Extremes.max / yAxis1MaxMinRatio).toFixed(0);
            // chart.yAxis[0].setExtremes(yAxis0Min, yAxis0Extremes.max);


          }
        };

        vm.chartConfig2.series.push({
          name: 'Irradiation',
          type: 'spline',
          yAxis: 1,
          data: data.irradiation_kwh,
          color: Highcharts.getOptions().colors[0],
          tooltip: {
            valueSuffix: ' w/m2'
          }
        });

        var i = 1;
        data.inverters.forEach(function (inverter) {
          var obj = {
            name: inverter.key,
            type: 'spline',
            data: inverter.value,
            visible: inverter.visible,
            color: Highcharts.getOptions().colors[i],
            tooltip: {
              valueSuffix: ' kW'
            }
          };
          vm.chartConfig2.series.push(obj);

          i = i + 1;
        });
      }

      function getInvertersGraph() {
        if (angular.isDefined(vm.chartConfig2)) {
          vm.chartConfig2.loading = true;
        }

        var requestPayload = {
          graphType: 'inverter',
          date: vm.date2
        };

        ApiService.GetById(requestPayload, '/powerPlants/graph/' + $stateParams.id).then(function (res) {
          if (res.success) {
            updateInvertersCharts(res.data);
            if (angular.isDefined(vm.chartConfig2)) {
              vm.chartConfig2.loading = false;
            }
          }
        });
      }

      // Update energy graph when date changed
      function date1Change() {
        getEnergyAndPower();
      }

      // Update energy graph when date changed
      function date2Change() {
        getInvertersGraph();
      }

      function date3Change() {
        getRawData();
      }

      function incDecEnergyDate(opr) {
        var localDate = new Date(vm.date1);
        if (opr == '-') {
          if (vm.type == 'month') {
            vm.date1 = new Date(localDate.setMonth(localDate.getMonth() - 1));
          } else {
            vm.date1 = new Date(localDate.setFullYear(localDate.getFullYear() - 1));
          }
        } else {
          if (vm.type == 'month') {
            vm.date1 = new Date(localDate.setMonth(localDate.getMonth() + 1));
          } else {
            vm.date1 = new Date(localDate.setFullYear(localDate.getFullYear() + 1));
          }
        }

        getEnergyAndPower();
      }

      function incDecInvDate(opr) {
        var localDate = new Date(vm.date2);
        if (opr == '-') {
          vm.date2 = new Date(localDate.setDate(localDate.getDate() - 1));
        } else {
          vm.date2 = new Date(localDate.setDate(localDate.getDate() + 1));
        }

        getInvertersGraph();
      }

      function incDecRawDate(opr) {
        var localDate = new Date(vm.date3);
        if (opr == '-') {
          vm.date3 = new Date(localDate.setDate(localDate.getDate() - 1));
        } else {
          vm.date3 = new Date(localDate.setDate(localDate.getDate() + 1));
        }
      }


      function setGraphType(type) {
        vm.type = type;

        if (type == 'month') {
          vm.dateOptions1.minMode = type;
          vm.format1 = 'MMMM yyyy';
        } else if (type == 'year') {
          vm.dateOptions1.minMode = type;
          vm.format1 = 'yyyy';
        }

        getEnergyAndPower();
      }

      function drawChart() {
        getEnergyAndPower();
        getInvertersGraph();
      }

      /**
       * GET Remote ShutDown Data
       */

      function remoteShutDownData() {
        var requestPayload = {
          projection: {"createdAt": 0, "updatedAt": 0, "isDeleted": 0, "__v": 0, "deviceMacLength": 0},
          criteria: {},
          options: {}
        };

        ApiService.GetById(requestPayload, '/powerPlants/remoteShutDownDetails/' + $stateParams.id).then(function (res) {
          if (res.success) {
            vm.remoteShutData = res.data;
            vm.remoteShutData.forEach(function (data, i) {
              data.isLoaded = false;
              $timeout(function () {
                getCurrentCapacity(data);
              }, 3000 * (i));
            });
          } else {
            $state.go('app.dashboard');
          }
        });
      }


      function getCurrentCapacity(data) {
        var requestPayload = {};

        ApiService.GetById(requestPayload, '/inverterCapacity?macAddress=' + data.macAddress + '&deviceInverterCount=' + data.deviceInverterCount).then(function (res) {
          if (res.success) {
            data.isLoaded = true;
            res.data = res.data.split("%");
            data.value = res.data[0];
          }
        });
      }

      //on change of  checkbox
      vm.set = function (data) {
        vm.checked = true;
        PlantService.updatedArray.push(data);

      }

      vm.askPassword = function () {
        ngDialog.open({
          template: 'scripts/modules/plants/partials/_ask-password.html',
          showClose: false,
          closeByEscape: false,
          closeByDocument: false,
          controller: 'PasswordCtrl',
          controllerAs: 'vm',
          className: 'ngdialog-theme-default fs-dialog'
        });
      };


      /*
       RAW DATA FUNCTION'
       */
      function getRawData() {
        vm.isProcessing = true;
        ApiService.GetCSV('/powerPlants/' + $stateParams.id + '/reports/rawData?date=' + vm.date3).then(function (res) {
          if (res) {
            vm.isProcessing = false;
            ApiService.FileDownload(res, "raw-data");
          }
        });
      }

      // Delete Plant
      function deletePlant(path) {
        vm.message = {preText: 'Are you sure want to', midText: ' delete', postText: ' ?'};
        vm.updateData = {
          path: path
        };
        openDialog();
      }

      var openedDialog;

      function openDialog() {
        openedDialog = ngDialog.openConfirm({
          template: 'scripts/modules/shared/partials/confirm-popup.html',
          className: 'ngdialog-theme-default',
          scope: $scope
        });
      }

      function updateConfirm() {
        vm.isProcessing = true;
        ApiService.Delete(vm.updateData.path)
          .then(function (response) {
            vm.isProcessing = false;
            ngDialog.close(openedDialog);

            if (response.success) {
              toastr.success('Deleted successfully!');
              $state.go('app.dashboard');
            }
          });
      }
    }

  })();
