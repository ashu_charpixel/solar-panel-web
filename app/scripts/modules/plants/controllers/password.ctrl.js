/**
 * Created by salma on 19/9/16.
 */
(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .controller('PasswordCtrl', PasswordCtrl);

  PasswordCtrl.$inject = ['PlantService','DialogBoxService','$log', '$state', '$stateParams', 'ApiService', 'toastr','$timeout'];
  function PasswordCtrl(PlantService,DialogBoxService,$log, $state, $stateParams, ApiService, toastr,$timeout) {
    var vm = this;
    vm.counter = 0;
    vm.authPassword = function(password){
      vm.payload = {
        password:password
      }
      ApiService.Create( vm.payload,'/remoteShutDown/authentication').then(function (res) {
        if (res.success) {
          toastr.success('Password matched successfully!');
          PlantService.sharedObj.isProcessing  = true;
          vm.counter = PlantService.updatedArray.length;
          DialogBoxService.Close();
          PlantService.updatedArray.forEach(function(data,i){
            $timeout(function() {
              vm.updateValue(data);
            }, 3000 * (i));
          });

        }
        else{
          DialogBoxService.Close();
        }
      });

    }


    vm.updateValue = function(data){
      vm.payload = {
        macAddress:data.macAddress,
        value:data.value,
        deviceInverterCount:data.deviceInverterCount

      }
      ApiService.Update( vm.payload,'/inverterCapacity').then(function (res) {
        vm.counter -= 1;
        if (res.success) {
          toastr.success(data.inverter + ' value updated successfully!');
        }
        if(vm.counter === 0){
          PlantService.sharedObj.isProcessing  = false;
        }
      });
    }


  }
})();
