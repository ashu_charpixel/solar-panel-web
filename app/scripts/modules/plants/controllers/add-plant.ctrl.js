/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc add a power plant
 */

(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .controller('AddPlantCtrl', AddPlantCtrl);

  AddPlantCtrl.$inject = ['$state', 'ApiService'];
  function AddPlantCtrl($state, ApiService) {
    var vm = this;

    vm.currentPageState = $state.current.stateDesc;
    vm.payload = {
      inverterEnergyCalculation:'kwh_today'
    };

    // Function declarations goes here
    vm.addPlant = addPlant;
    vm.open1 = open1;
    vm.open2 = open2;

    vm.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(),
      startingDay: 1
    };

    function open1() {
      vm.popup1.opened = true;
    }

    function open2() {
      vm.popup2.opened = true;
    }

    vm.format = 'dd MMMM yyyy';

    vm.popup1 = {
      opened: false
    };

    vm.popup2 = {
      opened: false
    };
    
    // Add plants functions
    function addPlant() {
      vm.dataLoading = true;

      // Convert lat long to array and delete the latitude & longitude key
      var payload = angular.copy(vm.payload);
      delete payload.latitude;
      delete payload.longitude;
      payload.location = [vm.payload.latitude, vm.payload.longitude];


      ApiService.Create(payload, '/powerPlants')
        .then(function (response) {
          vm.dataLoading = false;
          if (response.success) {
            $state.go('app.addDevice', {id: response.data._id});
          }
        });
    }

  }

})();
