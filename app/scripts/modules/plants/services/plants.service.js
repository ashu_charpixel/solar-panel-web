/**
 * Created by salma on 19/9/16.
 */

/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc user related services
 */


(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .factory('PlantService', PlantService);

  PlantService.$inject = ['$http','ENV'];
  function PlantService($http,ENV) {

    var service = {};
    service.updatedArray = [];
    service.sharedObj = {
      isProcessing:false
    }

    return service;


  }

})();

