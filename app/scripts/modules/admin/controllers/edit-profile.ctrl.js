/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold function to edit profile
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('EditProfileCtrl', EditProfileCtrl);

  EditProfileCtrl.$inject = ['AdminService', '$rootScope', '$state', 'toastr'];
  function EditProfileCtrl(AdminService, $rootScope, $state, toastr) {
    var vm = this;

    //Scope variables goes here
    vm.currentPageState = $state.current.stateDesc;
    vm.data = null;

    //Function Declaration goes here
    vm.editProfile = editProfile;
    vm.changePassword = changePassword;

    initController();

    function initController() {
      loadCurrentUser();
    }

    function loadCurrentUser() {
      var payload = {
        projection: {"__v": 0,"password": 0, "accessToken": 0, "loginAttempts": 0, "access": 0, "userType": 0, "registrationDate": 0},
        criteria: {},
        options: {}
      };

      AdminService.GetById($rootScope.globals.currentAdmin._id,payload)
        .then(function (admin) {
          vm.data = admin.data;
        });
    }

    function editProfile() {
      vm.dataLoading = true;
      vm.data._id = $rootScope.globals.currentAdmin._id;
      AdminService.Update(vm.data)
        .then(function (response) {
          vm.dataLoading = false;

          if (response.success) {
            toastr.success('Updated successfully!');
          }
        });
    }

    function changePassword() {
      vm.dataLoading = true;

      var payload = {
        _id: $rootScope.globals.currentAdmin._id
      };

      payload.password = vm.password;
      payload.newPassword = vm.newPassword;

      AdminService.ChangePassword(payload)
        .then(function (response) {
          vm.dataLoading = false;

          if (response.success) {
            vm.password = null;
            vm.newPassword = null;
            vm.confirmNewPassword = null;

            toastr.success('Updated successfully!');
          }
        });
    }

  }

})();
