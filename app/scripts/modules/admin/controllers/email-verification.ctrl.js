/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold reset password function
 */

(function() {
    'use strict';

    angular
        .module('com.module.admin')
        .controller('EmailVerificationCtrl', EmailVerificationCtrl);

    EmailVerificationCtrl.$inject = ['AdminService', '$location', 'toastr'];

    function EmailVerificationCtrl(AdminService, $location, toastr) {
        var vm = this;
        vm.admin = {};
        console.log('12345');

        vm.emailVerified = false;
        vm.admin.token = $location.search().token;
        vm.verifyEmail = verifyEmail;

        function verifyEmail() {

        AdminService.emailVerification(vm.admin)
            .then(function(response) {
                if (response.success) {

                    vm.emailVerified = true;
                    // $location.search('token', null);
                    // $location.path('/');
                }
                vm.dataLoading = false;
            });
        }

    }

})();
