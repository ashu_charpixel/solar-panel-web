/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold reset password function
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .factory('AuthenticationService', AuthenticationService);

  AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', 'ENV'];
  function AuthenticationService($http, $cookieStore, $rootScope, ENV) {
    var baseUrl = ENV.apiUrl+'/auth';

    var service = {};

    service.Login = Login;
    service.Logout = Logout;
    service.SetCredentials = SetCredentials;
    service.ClearCredentials = ClearCredentials;

    return service;

    function Login(payload) {
      return $http.post(baseUrl + '/login', payload).then(handleSuccess, handleError);
    }

    function Logout(){
      return $http.put(baseUrl + '/logout').then(handleSuccess, handleError);
    }

    function SetCredentials(data) {
      $rootScope.globals = {
        currentAdmin: data
      };

      $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;  // jshint ignore:line
      $cookieStore.put('sl_globals', $rootScope.globals);
    }

    function ClearCredentials() {
      $rootScope.globals = {};
      $cookieStore.remove('sl_globals');
      $http.defaults.headers.common.Authorization = 'Bearer ';
    }

    // private functions

    function handleSuccess(res) {
      return res.data;
    }

    function handleError() {
      return {success: false}
    }
  }

})();
