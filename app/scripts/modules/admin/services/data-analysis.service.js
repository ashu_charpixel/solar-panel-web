/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc admin related services
 */


(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .factory('DataAnalysisService', DataAnalysisService);

  DataAnalysisService.$inject = ['$http','ENV'];
  function DataAnalysisService($http,ENV) {
    var baseUrl = ENV.apiUrl+'/admin';

    var service = {
      GetGraphDataByType: GetGraphDataByType,
      GetUserStats: GetUserStats,
      updateStats: updateStats
    };

    return service;

    function GetGraphDataByType(type,payload) {
      return $http.get(baseUrl + '/graphs' + type,{params: payload}).then(handleSuccess, handleError);
    }

    function GetUserStats(type) {
      return $http.get(baseUrl + '/dashboard/' + type).then(handleSuccess, handleError);
    }

    function updateStats() {
      return $http.put(baseUrl + '/dashboard').then(handleSuccess, handleError);
    }

    // private functions

    function handleSuccess(res) {
      return res.data;
    }

    function handleError() {
      return {success: false}
    }

  }

})();
