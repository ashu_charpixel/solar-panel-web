/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc user related services
 */


(function () {
  'use strict';

  angular
    .module('com.module.users')
    .factory('UserService', UserService);

  UserService.$inject = ['$http','ENV'];
  function UserService($http,ENV) {
    var baseUrl=ENV.apiUrl+'/admin/users/';

    var service = {
      GetAll: GetAll,
      GetById: GetById,
      GetByUsername: GetByUsername,
      Create: Create,
      Update: Update,
      Delete: Delete
    };

    return service;

    function GetAll() {
      return $http.get(baseUrl).then(handleSuccess, handleError);
    }

    function GetById(id,payload) {
      return $http.get(baseUrl+ id,{params: payload}).then(handleSuccess, handleError);
    }

    function GetByUsername(username) {
      return $http.get(baseUrl + username).then(handleSuccess,handleError);
    }

    function Create(user) {
      return $http.post(baseUrl, user).then(handleSuccess, handleError);
    }

    function Update(user) {
      return $http.put(baseUrl + user._id, user).then(handleSuccess, handleError);
    }

    function Delete(id) {
      return $http.delete(baseUrl + id).then(handleSuccess, handleError);
    }

    // private functions

    function handleSuccess(res) {
      return res.data;
    }

    function handleError() {
      return {success: false}
    }

  }

})();
