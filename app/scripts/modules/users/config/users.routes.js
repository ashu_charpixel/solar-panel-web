/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc User module of the application.
 */

(function () {
  'use strict';

  angular
    .module('com.module.users')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {

    // Now set up the states
    $stateProvider
      .state('app.users', {
        url: '/users',
        stateDesc: {bodyClass: 'user-page',title: 'All users', description: 'Listing of all users here'},
        templateUrl: 'scripts/modules/users/views/users.html',
        controller: 'UserCtrl',
        controllerAs: 'vm'
      })
      .state('app.editProfile', {
        url: '/users/:id',
        stateDesc: {bodyClass: 'profile',title: 'View User Profile', description: 'View particular user here'},
        templateUrl: 'scripts/modules/users/views/profile.html',
        controller: 'EditUserProfileCtrl',
        controllerAs: 'vm'
      })
      .state('app.mapView', {
        url: '/map-view',
        stateDesc: {bodyClass: 'map-page',title: 'Map View', description: 'See the details on map'},
        templateUrl: 'scripts/modules/users/views/map-view.html',
        controller: 'MapViewCtrl',
        controllerAs: 'vm'
      })
      .state('app.reports', {
        url: '/reports',
        stateDesc: {bodyClass: 'plant-page',title: 'Reports', description: 'See the reports here'},
        templateUrl: 'scripts/modules/users/views/reports.html',
        controller: 'ReportsCtrl',
        controllerAs: 'vm'
      });
  }

})();
