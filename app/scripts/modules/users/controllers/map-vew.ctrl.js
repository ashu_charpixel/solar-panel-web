/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold function for reports related
 */

(function () {
  'use strict';

  angular
    .module('com.module.users')
    .controller('MapViewCtrl', MapViewCtrl);

  MapViewCtrl.$inject = ['$state'];
  function MapViewCtrl($state) {
    var vm = this;

    //Scope variables goes here
    vm.currentPageState = $state.current.stateDesc;

  }

})();
