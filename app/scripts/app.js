/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc Main module of the application.
 */

(function () {
  'use strict';

  angular
    .module('app', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ui.router',
      'ngSanitize',
      'ngMaterial',
      'ngTouch',
      'ngDialog',
      'toastr',
      'ui.bootstrap',
      'ui.select',
      'datatables',
      'datatables.bootstrap',
      'datatables.columnfilter',
      'datatables.fixedheader',
      'config',
      'com.module.core',
      'com.module.shared',
      'com.module.admin',
      'com.module.users',
      'com.module.plants'
    ])
    .run(run);

  run.$inject = ['$rootScope','$location', '$state', '$cookieStore', '$http', 'AuthenticationService','toastr'];
  function run($rootScope,$location, $state, $cookieStore, $http, AuthenticationService, toastr) {
    // keep admin logged in after page refresh
    $rootScope.globals = $cookieStore.get('sl_globals') || {};

    if ($rootScope.globals.currentAdmin) {
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.globals.currentAdmin.token;  // jshint ignore:line
    }

    $rootScope.$on('$stateChangeStart', function (e, toState) {
      // redirect to login page if not logged in and trying to access a restricted page

      var isLoginRequire = true;

      var pageType = toState.name.split('.')[0];
      if (pageType == 'page') {
        isLoginRequire = false;
      }

      var loggedIn = $rootScope.globals.currentAdmin;

      if (isLoginRequire && !loggedIn) {
        $location.path('/');
      }
      else if (!isLoginRequire && loggedIn){
        $location.path('/app/dashboard');
      }

      $rootScope.bodyClass = toState.stateDesc.bodyClass;
    });

    $rootScope.$on('$stateChangeSuccess', function() {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    });

    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams,error) {
      $state.go("page.login", null, {reload: true});
    });

    $rootScope.$on('unauthorized', function() {
      toastr.clear();
      toastr.warning($cookieStore.get('responseErrorMsg'));
      AuthenticationService.ClearCredentials();
      $state.go('page.login');
    });

    $rootScope.$on('something_went_wrong', function() {
      toastr.clear();
      toastr.error($cookieStore.get('responseErrorMsg'));
    });

    $rootScope.$on('bad_request', function() {
      toastr.clear();
      toastr.error($cookieStore.get('responseErrorMsg'));
    });


  }

})();
